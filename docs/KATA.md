# Notes avant de démarrer l'exercice

Bien qu'il s'agisse d'un exercice de "refactoring", 
je me dois de souligner que dans le cadre d'un projet réel,
il aurait peut-être été recommandable de développer un adaptateur entre 
la méthode `TemplateManager::getTemplateComputed` et un moteur de gabarit 
tel que [`Twig`](https://github.com/twigphp/Twig) ou [`Blade`](https://laravel.com/docs/5.7/blade) ayant fait leur preuve.

# Déroulé de l'exécution du kata de réusinage

Les commandes suivantes sont à exécuter à partir de la racine du projet.

## Installer les pré-requis

Installer PHP (version officielles fournies par le système d'exploitation) 
ou basculer sur l'usage d'un conteneur téléchargé à partir de https://hub.docker.com/_/php/ 
en fonction des contraintes. 

Installer composer à partir du [site officiel](https://getcomposer.org/download/).

Installer git.

## Choisir un interpréteur PHP

Étant donné que le fichier d'installation des dépendances indique que PHP 5.5.9 est accepté 
(et bien que cette version soit depuis peu considérée comme obsolète),
je fais le choix de ne pas sélectionner une version de PHP supérieure à 7.0.

## Installer les dépendances

```
php composer.phar install --prefer-dist
```

## Lancer l'interprétation du script d'exemple

```
php -f example/example.php
```

## Initialiser le référentiel de la base de code

```
git init
git remote add origin git@bitbucket.org:divbequal0/kata-refacto.git
# ou préférer l'envoi des changements via HTTPS
```

## Envoyer les jeux de modifications vers un référentiel distant

```
# Générer une paire de clefs publique / privée SSH
ssh-keygen -f ~/.ssh/kata

# Modifier les permissions de la clef privée
# afin qu'elle ne soit pas accessible à d'autres utilisateurs que celui l'ayant généré
chmod 0400  ~/.ssh/kata

# Lancer un agent de gestion de clefs SSH
ssh-agent /bin/zsh  

# Ajouter la nouvelle clef privée généré à son porte-clef
ssh-add ~/.ssh/kata 

# On envoie les modifications après ajout des fichiers présents
# et copie de sa clef SSH publique générée auprès de l'hébergeur de référentiel git
cat ~/.ssh/kata.pub                     # récupération de la clef publique
git add -A                              # Ajout des fichiers à l'index
git commit -m 'Import refactoring kata'

# Ajouter les lignes suivantes au fichier de configuration SSH 
# ~/.ssh/config
Host bitbucket.org
    Hostname bitbucket.org
    User git
    IdentityFile ~/.ssh/kata

git push origin master
```

## Démarrer le réusinage en TDD (développement piloté par les tests) 
## en lançant le test du gestionnaire de gabarit (`TemplateManager`)

Nous avons la bonne surprise de constater que le test est vert. 
On peut démarrer une session de refonte en supposant que ce test sera bien utile.
En effet, lorsqu'on altére la valeur de retour de la méthode `TemplateManager->computeText`,
on peut constater que le test passe au rouge ce qui répond bien à nos attentes, 
à savoir qu'il doit échouer en cas de comportement inattendu.

```
./vendor/bin/phpunit ./tests/TemplateManagerTest.php
```

## Configurer un IDE (PHPStorm) afin de lancer les tests plus rapidement tout au long de la session

![Analysis](/docs/img/configuration-phpstorm-test.png)

## Ajouter mbstring en extension requises par l'application

```
php composer.phar require "ext-mbstring:*"
``` 

## Lancer les tests en continu afin de s'assurer que les comportements attendus sont toujours respectés

## Passer en revue des cas limites qui sembleraient avoir été omis par la couverture

 - Absence de site en présence d'un devis
 - Mots-réservés (`placeholder`) présents en début de gabarit (`template`)
 - Forcer la présence de l'extension PHP `mbstring` dès l'installation des dépendances avec `composer`.
