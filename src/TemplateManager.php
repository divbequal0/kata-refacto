<?php

/**
 * Should you want to add a new placeholder to be replaced by a parameter (composite or otherwise),
 * please consider beforehand
 *  - declaring a constant to hold the name of the covered topic, e.g. quote, user, departure
 *  - declaring constants *namespaced* by the topic name, e.g. [quote:summary_html], [user:first_name], [departure:airport]
 *  - ensuring the absence of a new placeholder does not break the current behavior (the `TemplateManager` test still passes)
 *  - ensuring the presence of a new placeholder is introduced for a template subject or content by declaring
 *  a method prefixed with `render`, e.g. renderDestinationName, renderDepartureAirport
 */
class TemplateManager
{
    const PLACEHOLDER_QUOTE = 'quote';
    const PLACEHOLDER_USER = 'user';
    const PLACEHOLDER_QUOTE_SUMMARY_HTML = '[quote:summary_html]';
    const PLACEHOLDER_QUOTE_SUMMARY = '[quote:summary]';
    const PLACEHOLDER_DESTINATION_LINK = '[quote:destination_link]';
    const PLACEHOLDER_USER_FIRST_NAME = '[user:first_name]';
    const PLACEHOLDER_DESTINATION_NAME = '[quote:destination_name]';

    const TEMPLATE_TYPE_CONTENT = 'content';
    const TEMPLATE_TYPE_SUBJECT = 'subject';

    private $templates = [];

    /**
     * @param Template $template
     * @param array    $templatingParameters
     * @return string|Template
     */
    public function getTemplateComputed(Template $template, array $templatingParameters)
    {
        if (!($template instanceof Template)) {
            throw new \RuntimeException(
                'Missing template. Please provide with an instance of \Template as first argument'
            );
        }

        try {
            return $this->renderTemplate($template, $templatingParameters);
        } catch (\Exception $exception) {
            error_log($exception->getMessage());

            return implode(
                "\n",
                [
                    'Sorry, an unexpected error has occurred. ',
                    'Please reach us via https://www.evaneos.fr/infos/nous-contacter/ ',
                    'so that we can fix this issue as quickly as possible and make it for you.'
                ]
            );
        }
    }

    /**
     * @param       $template
     * @param array $templatingParameters
     * @return mixed
     * @throws Exception
     */
    private function renderContent($template, array $templatingParameters)
    {
        $this->templates[self::TEMPLATE_TYPE_CONTENT] = $template;

        if ($this->shouldQuoteDetailsBeRendered($templatingParameters))
        {
            $template = $this->renderQuoteSummary($template, $templatingParameters);
            $template = $this->renderDestinationName(
                $template,
                $templatingParameters,
                self::TEMPLATE_TYPE_CONTENT
            );
        }

        $template = $this->renderDestinationLink($template, $templatingParameters);
        $template = $this->renderUserDetails($template, $templatingParameters);

        return $template;
    }

    /**
     * @param       $template
     * @param array $templatingParameters
     * @return mixed
     */
    private function renderSubject($template, array $templatingParameters)
    {
        $this->templates[self::TEMPLATE_TYPE_SUBJECT] = $template;

        $quoteDetails = $this->getQuoteDetails($templatingParameters);
        if (!$quoteDetails) {
            return $template;
        }

        return $this->renderDestinationName($template, $templatingParameters, self::TEMPLATE_TYPE_SUBJECT);
    }

    /**
     * @param array $templatingParameters
     * @return mixed|null
     */
    private function shouldQuoteDetailsBeRendered(array $templatingParameters)
    {
        return isset($templatingParameters[self::PLACEHOLDER_QUOTE]) and $templatingParameters[self::PLACEHOLDER_QUOTE] instanceof Quote;
    }

    /**
     * @param       $text
     * @param array $templatingParameters
     * @return mixed
     * @throws Exception
     */
    private function renderDestinationLink($text, array $templatingParameters)
    {
        $quote = null;
        if ($this->shouldQuoteDetailsBeRendered($templatingParameters))
        {
            $quoteDetails = $this->getQuoteDetails($templatingParameters);
            $quote = QuoteRepository::getInstance()->getById($quoteDetails->id);
        }

        $site = null;
        if (!is_null($quote)) {
            $site = SiteRepository::getInstance()->getById($quote->siteId);
        }

        $destination = $this->getDestination($templatingParameters);
        $this->guardAgainstMissingSite($site, $destination);

        if (is_null($destination)) {
            return str_replace(self::PLACEHOLDER_DESTINATION_LINK, '', $text);
        }

        return str_replace(self::PLACEHOLDER_DESTINATION_LINK, implode('/', [
            $site->url ,
            $destination->countryName ,
            'quote' ,
            $quote->id]),
            $text
        );
    }

    /**
     * @param $site
     * @param $destination
     * @throws Exception
     */
    private function guardAgainstMissingSite($site, $destination)
    {
        if (!($site instanceof Site) && !is_null($destination)) {
            throw new \Exception('A valid site is required. Please provide with a quote having a site id.');
        }
    }

    /**
     * @param Template $template
     * @param array    $parameters
     * @return Template
     * @throws Exception
     */
    private function renderTemplate(Template $template, array $parameters)
    {
        $renderedTemplate = clone($template);

        $renderedTemplate->content = $this->renderContent($renderedTemplate->content, $parameters);
        $renderedTemplate->subject = $this->renderSubject($renderedTemplate->subject, $parameters);

        return $renderedTemplate;
    }

    /**
     * @param string $templateName
     * @param string $placeholder
     * @return bool
     */
    private function templateContains($templateName, $placeholder)
    {
        return strpos($this->templates[$templateName], $placeholder) !== false;
    }

    /**
     * @param string $templateName
     * @param string $placeholder
     * @return bool
     */
    private function templateDoesNotContain($templateName, $placeholder)
    {
        return !$this->templateContains($templateName, $placeholder);
    }

    /**
     * @param array $templatingParameters
     * @return mixed|User
     */
    private function getUser(array $templatingParameters)
    {
        if (isset($templatingParameters[self::PLACEHOLDER_USER]) and ($templatingParameters[self::PLACEHOLDER_USER] instanceof User)) {
            return $templatingParameters[self::PLACEHOLDER_USER];
        }

        $applicationContext = ApplicationContext::getInstance();

        return $applicationContext->getCurrentUser();
    }

    /**
     * @param array $templatingParameters
     * @return mixed|null
     */
    private function getQuoteDetails(array $templatingParameters)
    {
        if ($this->shouldQuoteDetailsBeRendered($templatingParameters)) {
            return $templatingParameters[self::PLACEHOLDER_QUOTE];
        }

        return null;
    }

    /**
     * @param array $templatingParameters
     * @return Destination|null
     */
    private function getDestination(array $templatingParameters)
    {
        $destination = null;
        $quoteDetails = $this->getQuoteDetails($templatingParameters);
        if ($this->shouldQuoteDetailsBeRendered($templatingParameters) &&
            $this->templateContains(self::TEMPLATE_TYPE_CONTENT, self::PLACEHOLDER_DESTINATION_LINK)) {
            $quote = QuoteRepository::getInstance()->getById($quoteDetails->id);
            $destination = DestinationRepository::getInstance()->getById($quote->destinationId);
        }

        return $destination;
    }

    /**
     * @param $contentTemplate
     * @param $applyTransformationBeforeInsertion
     * @param $placeholder
     * @return mixed
     */
    private function applyTransformationBeforeInsertion(callable $applyTransformationBeforeInsertion, $contentTemplate, $placeholder)
    {
        if ($this->templateDoesNotContain(self::TEMPLATE_TYPE_CONTENT, $placeholder)) {
            return $contentTemplate;
        }

        return str_replace(
            $placeholder,
            $applyTransformationBeforeInsertion,
            $contentTemplate
        );
    }

    /**
     * @param       $contentTemplate
     * @param array $templatingParameters
     * @return mixed
     */
    private function renderUserDetails($contentTemplate, array $templatingParameters)
    {
        $user = $this->getUser($templatingParameters);
        if ($user instanceof User &&
            $this->templateContains(self::TEMPLATE_TYPE_CONTENT, self::PLACEHOLDER_USER_FIRST_NAME)) {
            $contentTemplate = str_replace(self::PLACEHOLDER_USER_FIRST_NAME, ucfirst(mb_strtolower($user->firstname)), $contentTemplate);
        }

        return $contentTemplate;
    }

    /**
     * @param       $contentTemplate
     * @param array $templatingParameters
     * @return mixed
     */
    private function renderQuoteSummary($contentTemplate, array $templatingParameters)
    {
        $quoteDetails = $this->getQuoteDetails($templatingParameters);
        $quote = QuoteRepository::getInstance()->getById($quoteDetails->id);

        $contentTemplate = $this->applyTransformationBeforeInsertion(
            $applyTransformationBeforeInsertion = function () use ($quote) {
                return Quote::renderHtml($quote);
            },
            $contentTemplate,
            self::PLACEHOLDER_QUOTE_SUMMARY_HTML
        );

        $contentTemplate = $this->applyTransformationBeforeInsertion(
            function () use ($quote) {
                return Quote::renderText($quote);
            },
            $contentTemplate,
            self::PLACEHOLDER_QUOTE_SUMMARY
        );

        return $contentTemplate;
    }

    /**
     * @param       $template
     * @param array $templatingParameters
     * @param       $templateName
     * @return mixed
     */
    private function renderDestinationName($template, array $templatingParameters, $templateName)
    {
        if ($this->templateContains($templateName, self::PLACEHOLDER_DESTINATION_NAME)) {
            $quoteDetails = $this->getQuoteDetails($templatingParameters);
            $quote = QuoteRepository::getInstance()->getById($quoteDetails->id);
            $destinationOfQuote = DestinationRepository::getInstance()->getById($quote->destinationId);

            return str_replace(self::PLACEHOLDER_DESTINATION_NAME, $destinationOfQuote->countryName, $template);
        }

        return $template;
    }
}
